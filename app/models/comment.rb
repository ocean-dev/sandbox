# == Schema Information
#
# Table name: comments
#
#  id           :string           primary key
#  note_id      :string
#  body         :text
#  created_by   :string
#  updated_by   :string
#  lock_version :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_comments_on_id       (id) UNIQUE
#  index_comments_on_note_id  (note_id)
#

class Comment < ActiveRecord::Base

  self.primary_key = "id"

  # Callbacks
  def initialize(*)
    super
    self.id ||= SecureRandom.uuid
  end


  ocean_resource_model index: [:body], search: :body


  # Relations
  belongs_to :note


  # Attributes
  attr_accessible :lock_version, :body


  # Validations
  validates :body, presence: true
  validates :note_id, presence: true
  
end
