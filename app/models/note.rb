# == Schema Information
#
# Table name: notes
#
#  id           :string           primary key
#  title        :string
#  body         :text
#  created_by   :string
#  updated_by   :string
#  lock_version :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_notes_on_id  (id) UNIQUE
#

class Note < ActiveRecord::Base

  self.primary_key = "id"

  # Callbacks
  def initialize(*)
    super
    self.id ||= SecureRandom.uuid
  end


  ocean_resource_model index: [:title], search: :body


  # Relations
  has_many :comments, dependent: :destroy


  # Attributes
  attr_accessible :lock_version, :title, :body


  # Validations
  validates :title, presence: true
  
end
