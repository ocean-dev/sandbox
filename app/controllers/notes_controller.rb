class NotesController < ApplicationController

  ocean_resource_controller extra_actions: {'comments' =>       ['comments', "GET"],
                                            'comment_create' => ['comments', "POST"]}
  
  before_action :find_note, :only => [:show, :update, :destroy, :comments, :comment_create]
    
  
  # GET /notes
  def index
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(collection_etag(Note))
      api_render Note.collection(params)
    end
  end


  # GET /notes/1
  def show
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(@note)
      api_render @note
    end
  end


  # POST /notes
  def create
    @note = Note.new(filtered_params Note)
    set_updater(@note)
    @note.save!
    api_render @note, new: true
  end


  # PUT /notes/1
  def update
    if missing_attributes?
      render_api_error 422, "Missing resource attributes"
      return
    end
    @note.assign_attributes(filtered_params Note)
    set_updater(@note)
    @note.save!
    api_render @note
  end


  # DELETE /notes/1
  def destroy
    @note.destroy
    render_head_204
  end


  # GET /notes/1/comments
  def comments
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(@note.comments)
      api_render @note.comments
    end
  end


  # POST /notes/1/comments
  def comment_create
    @comment = @note.comments.new(filtered_params Comment)
    set_updater(@comment)
    @comment.save!
    api_render @comment, new: true
  end

  
  private
     
  def find_note
    @note = Note.find_by_id params[:id]
    return true if @note
    render_api_error 404, "Note not found"
    false
  end
    
end
