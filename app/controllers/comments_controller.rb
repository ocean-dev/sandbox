class CommentsController < ApplicationController

  ocean_resource_controller
  
  before_action :find_comment, :only => [:show, :update, :destroy]
    
  
  # GET /comments
  def index
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(collection_etag(Comment))
      api_render Comment.collection(params)
    end
  end


  # GET /comments/1
  def show
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(@comment)
      api_render @comment
    end
  end


  # PUT /comments/1
  def update
    if missing_attributes?
      render_api_error 422, "Missing resource attributes"
      return
    end
    @comment.assign_attributes(filtered_params Comment)
    set_updater(@comment)
    @comment.save!
    api_render @comment
  end


  # DELETE /comments/1
  def destroy
    @comment.destroy
    render_head_204
  end


  private
     
  def find_comment
    @comment = Comment.find_by_id params[:id]
    return true if @comment
    render_api_error 404, "Comment not found"
    false
  end
    
end
