json.note do |json|
	json._links       hyperlinks(self:     note_url(note),
                                 comments: comments_note_url(note),
	                             creator:  smart_api_user_url(note.created_by),
	                             updater:  smart_api_user_url(note.updated_by))
	json.(note, :lock_version, :title, :body) 
	json.created_at   note.created_at.utc.iso8601
	json.updated_at   note.updated_at.utc.iso8601
end
