json.comment do |json|
	json._links       hyperlinks(self:    comment_url(comment),
                                 note:    note_url(comment.note),
	                             creator: smart_api_user_url(comment.created_by),
	                             updater: smart_api_user_url(comment.updated_by))
	json.(comment, :lock_version, :body) 
	json.created_at   comment.created_at.utc.iso8601
	json.updated_at   comment.updated_at.utc.iso8601
end
