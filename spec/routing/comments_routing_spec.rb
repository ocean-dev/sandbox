require "spec_helper"

describe CommentsController do
  describe "routing" do

    it "routes to #index" do
      expect(get("/v1/comments")).to route_to("comments#index")
    end

    it "routes to #show" do
      expect(get("/v1/comments/1-2-3-4-5")).to route_to("comments#show", :id => "1-2-3-4-5")
    end

    it "routes to #create" do
      expect(post("/v1/comments")).not_to be_routable
    end

    it "routes to #update" do
      expect(put("/v1/comments/1-2-3-4-5")).to route_to("comments#update", :id => "1-2-3-4-5")
    end

    it "routes to #destroy" do
      expect(delete("/v1/comments/1-2-3-4-5")).to route_to("comments#destroy", :id => "1-2-3-4-5")
    end

  end
end
