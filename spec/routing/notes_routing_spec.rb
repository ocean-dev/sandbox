require "spec_helper"

describe NotesController do
  describe "routing" do

    it "routes to #index" do
      expect(get("/v1/notes")).to route_to("notes#index")
    end

    it "routes to #show" do
      expect(get("/v1/notes/1-2-3-4-5")).to route_to("notes#show", :id => "1-2-3-4-5")
    end

    it "routes to #create" do
      expect(post("/v1/notes")).to route_to("notes#create")
    end

    it "routes to #update" do
      expect(put("/v1/notes/1-2-3-4-5")).to route_to("notes#update", :id => "1-2-3-4-5")
    end

    it "routes to #destroy" do
      expect(delete("/v1/notes/1-2-3-4-5")).to route_to("notes#destroy", :id => "1-2-3-4-5")
    end

    it "routes to #comments" do
      expect(get("/v1/notes/1-2-3-4-5/comments")).to route_to("notes#comments", id: "1-2-3-4-5")
    end

    it "routes to #comment_create" do
      expect(post("/v1/notes/1-2-3-4-5/comments")).to route_to("notes#comment_create", id: "1-2-3-4-5")
    end

  end
end
