# == Schema Information
#
# Table name: comments
#
#  id           :string           primary key
#  note_id      :string
#  body         :text
#  created_by   :string
#  updated_by   :string
#  lock_version :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_comments_on_id       (id) UNIQUE
#  index_comments_on_note_id  (note_id)
#

require 'spec_helper'

describe Comment do


  describe "attributes" do
    
    it "should have a body" do
      expect(create(:comment).body).to be_a String
    end

     it "should require a body" do
      expect(build(:comment, body: nil)).not_to be_valid
      expect(build(:comment, body: "")).not_to be_valid
      expect(build(:comment, body: " ")).not_to be_valid
    end

    it "should have a creation time" do
      expect(create(:comment).created_at).to be_a Time
    end

    it "should have an update time" do
      expect(create(:comment).updated_at).to be_a Time
    end
  
   it "should have a creator" do
      expect(create(:comment).created_by).to be_a String
    end

    it "should have an updater" do
      expect(create(:comment).updated_by).to be_a String
    end

  end


  describe "relations" do

    it "should always be associated with a Note" do
      expect(build(:comment, note: nil)).not_to be_valid
    end

  end



  describe "search" do
  
    describe ".collection" do
    
      before :each do
        create :comment, body: "The Foo object"
        create :comment, body: "The Bar object"
        create :comment, body: "The Baz object"
      end
      
    
      it "should return an array of Comment instances" do
        ix = Comment.collection
        expect(ix.length).to eq(3)
        expect(ix[0]).to be_a Comment
      end
    
      it "should allow matches on body" do
        expect(Comment.collection(body: 'NOWAI').length).to eq(0)
        expect(Comment.collection(body: 'The Bar object').length).to eq(1)
        expect(Comment.collection(body: 'The Baz object').length).to eq(1)
      end
      
      it "should allow searches on description" do
        expect(Comment.collection(search: 'a').length).to eq(2)
        expect(Comment.collection(search: 'object').length).to eq(3)
      end
      
      it "key/value pairs not in the index_only array should quietly be ignored" do
        expect(Comment.collection(body: 'The Bar object', aardvark: 12).length).to eq(1)
      end
        
    end
  end

end
