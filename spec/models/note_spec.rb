# == Schema Information
#
# Table name: notes
#
#  id           :string           primary key
#  title        :string
#  body         :text
#  created_by   :string
#  updated_by   :string
#  lock_version :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_notes_on_id  (id) UNIQUE
#

require 'spec_helper'

describe Note do


  describe "attributes" do
    
    it "should have a title" do
      expect(create(:note).title).to be_a String
    end

    it "should require a title" do
      expect(build(:note, title: nil)).not_to be_valid
      expect(build(:note, title: "")).not_to be_valid
      expect(build(:note, title: " ")).not_to be_valid
    end

    it "should have a body" do
      expect(create(:note).body).to be_a String
    end

     it "should have a creation time" do
      expect(create(:note).created_at).to be_a Time
    end

    it "should have an update time" do
      expect(create(:note).updated_at).to be_a Time
    end
  
   it "should have a creator" do
      expect(create(:note).created_by).to be_a String
    end

    it "should have an updater" do
      expect(create(:note).updated_by).to be_a String
    end

  end


  describe "relations" do

    it "should include a collection of Comments" do
      expect(create(:note).comments).to eq([])
    end

    it "should destroy its Comments when itself is destroyed" do
      note = create :note
      c1 = create :comment, note: note
      c2 = create :comment, note: note
      c3 = create :comment, note: note
      note.destroy
      expect(Comment.count).to eq(0)
    end

  end



  describe "search" do
  
    describe ".collection" do
    
      before :each do
        create :note, title: 'foo', body: "The Foo object"
        create :note, title: 'bar', body: "The Bar object"
        create :note, title: 'baz', body: "The Baz object"
      end
      
    
      it "should return an array of Note instances" do
        ix = Note.collection
        expect(ix.length).to eq(3)
        expect(ix[0]).to be_a Note
      end
    
      it "should allow matches on name" do
        expect(Note.collection(title: 'NOWAI').length).to eq(0)
        expect(Note.collection(title: 'bar').length).to eq(1)
        expect(Note.collection(title: 'baz').length).to eq(1)
      end
      
      it "should allow searches on description" do
        expect(Note.collection(search: 'a').length).to eq(2)
        expect(Note.collection(search: 'object').length).to eq(3)
      end
      
      it "key/value pairs not in the index_only array should quietly be ignored" do
        expect(Note.collection(title: 'bar', aardvark: 12).length).to eq(1)
      end
        
    end
  end

end
