require 'spec_helper'

describe NotesController do
  
  render_views

  describe "GET comments" do
    
    before :each do
      permit_with 200
      @n1 = create :note
      @c1 = create :comment, note: @n1
      @c2 = create :comment, note: @n1
      @n2 = create :note
      @c3 = create :comment, note: @n2
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "boy-is-this-fake"
    end
    
    
    it "should return JSON" do
      get :comments, id: @n1
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      get :comments, id: @n1
      expect(response.status).to eq(400)
      expect(response.content_type).to eq("application/json")
    end

    it "should return a 404 when the Note can't be found" do
      get :comments, id: "0-0-0-0-0"
      expect(response.status).to eq(404)
      expect(response.content_type).to eq("application/json")
    end

    it "should return a 200 when successful" do
      get :comments, id: @n1
      expect(response).to render_template(partial: "comments/_comment", count: 2)
      expect(response.status).to eq(200)
    end
    
    it "should return a collection" do
      get :comments, id: @n1
      expect(response.status).to eq(200)
      wrapper = JSON.parse(response.body)
      expect(wrapper).to be_a Hash
      resource = wrapper['_collection']
      expect(resource).to be_a Hash
      coll = resource['resources']
      expect(coll).to be_an Array
      expect(coll.count).to eq(2)
      n = resource['count']
      expect(n).to eq(2)
    end

  end
  
end
