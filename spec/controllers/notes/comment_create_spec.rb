require 'spec_helper'

describe NotesController do
  
  render_views
  
  describe "POST" do
    
    before :each do
      permit_with 200
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "incredibly-fake!"
      @note = create :note
      c = build :comment, note: @note
      @args = { 'id' => @note.id, 'body' => "The body." }
    end

    
    it "should return JSON" do
      post :comment_create, @args
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      post :comment_create, @args
      expect(response.status).to eq(400)
    end
    
    it "should return a 404 when the Note can't be found" do
      post :comment_create, id: "0-0-0-0-0"
      expect(response.status).to eq(404)
      expect(response.content_type).to eq("application/json")
    end

    it "should return a 422 when there are validation errors" do
      post :comment_create, @args.merge('body' => "      ")
      expect(response.status).to eq(422)
      expect(response.content_type).to eq("application/json")
      expect(JSON.parse(response.body)).to eq({"body"=>["can't be blank"]})
    end
                
    it "should return a 201 when successful" do
      post :comment_create, @args
      expect(response).to render_template(partial: "comments/_comment", count: 1)
      expect(response.status).to eq(201)
    end

    it "should contain a Location header when successful" do
      post :comment_create, @args
      expect(response.headers['Location']).to be_a String
    end

    it "should return the new resource in the body when successful" do
      post :comment_create, @args
      expect(response.body).to be_a String
    end

    it "should increase the number of associated Comments for the Note by one" do
      expect(@note.comments.count).to eq(0)
      post :comment_create, @args
      expect(@note.comments.count).to eq(1)
    end

  end
  
end
