require 'spec_helper'

describe NotesController do
  
  render_views

  describe "DELETE" do
    
    before :each do
      permit_with 200
      @note = create :note
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "so-totally-fake"
    end

    
    it "should return JSON" do
      delete :destroy, id: @note
      expect(response.content_type).to eq("application/json")
    end

    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      delete :destroy, id: @note
      expect(response.status).to eq(400)
    end
    
    it "should return a 204 when successful" do
      delete :destroy, id: @note
      expect(response.status).to eq(204)
      expect(response.content_type).to eq("application/json")
    end

    it "should return a 404 when the Note can't be found" do
      delete :destroy, id: "0-0-0-0-0"
      expect(response.status).to eq(404)
    end
    
    it "should destroy the Note when successful" do
      delete :destroy, id: @note
      expect(response.status).to eq(204)
      expect(Note.find_by_id(@note.id)).to be_nil
    end
    
  end
  
end
