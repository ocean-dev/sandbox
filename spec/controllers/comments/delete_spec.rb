require 'spec_helper'

describe CommentsController do
  
  render_views

  describe "DELETE" do
    
    before :each do
      permit_with 200
      @comment = create :comment
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "so-totally-fake"
    end

    
    it "should return JSON" do
      delete :destroy, id: @comment
      expect(response.content_type).to eq("application/json")
    end

    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      delete :destroy, id: @comment
      expect(response.status).to eq(400)
    end
    
    it "should return a 204 when successful" do
      delete :destroy, id: @comment
      expect(response.status).to eq(204)
      expect(response.content_type).to eq("application/json")
    end

    it "should return a 404 when the Comment can't be found" do
      delete :destroy, id: "0-0-0-0-0"
      expect(response.status).to eq(404)
    end
    
    it "should destroy the Comment when successful" do
      delete :destroy, id: @comment
      expect(response.status).to eq(204)
      expect(Comment.find_by_id(@comment.id)).to be_nil
    end
    
  end
  
end
