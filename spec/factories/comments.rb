# == Schema Information
#
# Table name: comments
#
#  id           :string           primary key
#  note_id      :string
#  body         :text
#  created_by   :string
#  updated_by   :string
#  lock_version :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_comments_on_id       (id) UNIQUE
#  index_comments_on_note_id  (note_id)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :comment do
    note
    body "MyText"
    created_by "https://api.example.com/v1/api_users/a-b-c-d-e"
    updated_by "https://api.example.com/v1/api_users/a-b-c-d-e"
    lock_version 1
  end
end
