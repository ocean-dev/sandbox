# == Schema Information
#
# Table name: notes
#
#  id           :string           primary key
#  title        :string
#  body         :text
#  created_by   :string
#  updated_by   :string
#  lock_version :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_notes_on_id  (id) UNIQUE
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :note do
    title "MyString"
    body "MyText"
    created_by "https://api.example.com/v1/api_users/2-3-4-5-6"
    updated_by "https://api.example.com/v1/api_users/8-9-a-b-c"
    lock_version 1
  end
end
