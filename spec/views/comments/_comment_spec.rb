require 'spec_helper'

describe "comments/_comment" do
  
  before :each do                     # Must be :each (:all causes all tests to fail)
    render partial: "comments/comment", locals: {comment: create(:comment)}
    @json = JSON.parse(rendered)
    @u = @json['comment']
    @links = @u['_links'] rescue {}
  end


  it "has a named root" do
    expect(@u).not_to eq(nil)
  end


  it "should have four hyperlinks" do
    expect(@links.size).to eq(4)
  end

  it "should have a self hyperlink" do
    expect(@links).to be_hyperlinked('self', /comments/)
  end

  it "should have a creator hyperlink" do
    expect(@links).to be_hyperlinked('creator', /api_users/)
  end

  it "should have an updater hyperlink" do
    expect(@links).to be_hyperlinked('updater', /api_users/)
  end

  it "should have a note hyperlink" do
    expect(@links).to be_hyperlinked('note', /notes/)
  end


  it "should have a body" do
    expect(@u['body']).to be_a String
  end

  it "should have a created_at time" do
    expect(@u['created_at']).to be_a String
  end

  it "should have an updated_at time" do
    expect(@u['updated_at']).to be_a String
  end

  it "should have a lock_version field" do
    expect(@u['lock_version']).to be_an Integer
  end
      
end
