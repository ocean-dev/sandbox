class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments, id: false do |t|
      t.string :id
      t.string :note_id
      t.text :body
      t.string :created_by
      t.string :updated_by
      t.integer :lock_version, null: false, default: 0

      t.timestamps
    end
    add_index :comments, :id, unique: true
    add_index :comments, :note_id
  end
end
