class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes, id: false do |t|
      t.string :id
      t.string :title
      t.text :body
      t.string :created_by
      t.string :updated_by
      t.integer :lock_version, null: false, default: 0
      t.timestamps
    end
    add_index :notes, :id, unique: true
  end
end
