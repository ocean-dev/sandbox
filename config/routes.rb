Sandbox::Application.routes.draw do

  scope "v1" do

    resources :notes, except: [:new, :edit],
                      constraints: {id: UUID_REGEX} do
      member do
        get  'comments'
        post 'comments' => 'notes#comment_create'      
      end
    end

    resources :comments, except: [:new, :edit, :create],
                         constraints: {id: UUID_REGEX}

  end

end
